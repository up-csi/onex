Rails.application.routes.draw do
  devise_for :admins
  devise_for :users
  
  resource :dashboard, controller: :dashboard, only: [:show]

  root 'static_pages#home'
end
